#include "eadkpp.h"
#include "palette.h"
#include "storage.h"
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <string.h>

#define MAX_FILES 30

extern const char eadk_app_name[] __attribute__((section(".rodata.eadk_app_name"))) = "Storage";
extern const uint32_t eadk_api_level __attribute__((section(".rodata.eadk_api_level"))) = 0;

void waitForExe() {
  // Don't exit unless EXE or Home is pressed
  while (true) {
    EADK::Keyboard::State keyboardState = EADK::Keyboard::scan();
    if (keyboardState.keyDown(EADK::Keyboard::Key::EXE)) {
      return;
    }
    if (keyboardState.keyDown(EADK::Keyboard::Key::Home)) {
      return;
    }
    EADK::Timing::msleep(20);
  }
}

void waitForExeRelease() {
  // Don't exit unless EXE and Home are released
  while (true) {
    EADK::Keyboard::State keyboardState = EADK::Keyboard::scan();
    if ((!keyboardState.keyDown(EADK::Keyboard::Key::EXE)) && (!keyboardState.keyDown(EADK::Keyboard::Key::Home))) {
      EADK::Timing::msleep(20);
      EADK::Keyboard::scan();
      return;
    }
    EADK::Timing::msleep(20);
  }
}


int listFiles() {
  EADK::Display::pushRectUniform(EADK::Screen::Rect, Black);

  // The file list
  char * filenames[MAX_FILES];

  int nb = extapp_fileList((const char **)filenames, MAX_FILES, "");

  EADK::Point location(0, 0);
  if (nb == -1) {
    EADK::Display::pushRectUniform(EADK::Screen::Rect, Red);
    EADK::Display::drawString("Error", location, true, Black, Red);
    waitForExe();
    waitForExeRelease();
    return 1;
  }

  for (int fileIndex = 0; fileIndex < nb; fileIndex++) {
    EADK::Point location(0, 15 * fileIndex);
    EADK::Display::drawString(filenames[fileIndex], location, false, White, Black);
  }

  return 0;
}

int checkFile() {
  EADK::Display::pushRectUniform(EADK::Screen::Rect, Black);

  EADK::Point location(0, 0);
  if (extapp_fileExists("exists.py")) {
    EADK::Display::drawString("'exists'.py' found", location, true, Black, White);
  } else {
    EADK::Display::drawString("'exists'.py' NOT found", location, true, Black, Red);
  }

  return 0;
}

int readFile() {
  EADK::Display::pushRectUniform(EADK::Screen::Rect, Black);

  // We read "read.py"
  size_t file_len = 0;
  const char * content = extapp_fileRead("read.py", &file_len);

  EADK::Point location(0, 0);
  if (content == NULL) {
    EADK::Display::pushRectUniform(EADK::Screen::Rect, Red);
    EADK::Display::drawString("'read.py' not found", location, true, Black, Red);
    waitForExe();
    waitForExeRelease();
    return 1;
  }

  // The file is found, so we draw his content (content + 1 is for the autoimport status)
  EADK::Display::drawString(content + 1, location, false, White, Black);
  return 0;
}

int writeFile() {
  EADK::Display::pushRectUniform(EADK::Screen::Rect, Black);

  // We write "write.py"
  const char * content = "\0This is a wonderful content with autoimport diasbled";
  // First char is \0 (autoimport), so we skip it, then the strlen, then the final \0
  size_t len = 1 + strlen(content + 1) + 1;
  bool succeed =  extapp_fileWrite("write.py", content, len);

  EADK::Point location(0, 0);
  if (!succeed) {
    EADK::Display::pushRectUniform(EADK::Screen::Rect, Red);
    EADK::Display::drawString("Error writing 'write.py'", location, true, Black, Red);
    waitForExe();
    waitForExeRelease();
    return 1;
  }

  EADK::Display::drawString("File 'write.py' written successfully !", location, false, White, Black);
  return 0;
}

int deleteFile() {
  EADK::Display::pushRectUniform(EADK::Screen::Rect, Black);

  // We delete "delete.py"
  bool succeed =  extapp_fileErase("delete.py");

  EADK::Point location(0, 0);
  if (!succeed) {
    EADK::Display::pushRectUniform(EADK::Screen::Rect, Red);
    EADK::Display::drawString("Error deleting 'delete.py'", location, true, Black, Red);
    waitForExe();
    waitForExeRelease();
    return 1;
  }

  EADK::Display::drawString("File 'delete.py' deleted successfully !", location, false, White, Black);
  return 0;
}

int testStorage() {
  int returnValue = 0;

  // List files
  returnValue = listFiles();
  if (returnValue != 0) {
    return returnValue;
  }
  waitForExe();
  waitForExeRelease();

  // Check existence of a file
  returnValue = checkFile();
  if (returnValue != 0) {
    return returnValue;
  }
  waitForExe();
  waitForExeRelease();

  // Read a file
  returnValue = readFile();
  if (returnValue != 0) {
    return returnValue;
  }
  waitForExe();
  waitForExeRelease();

  // Write a file
  returnValue = writeFile();
  if (returnValue != 0) {
    return returnValue;
  }
  waitForExe();
  waitForExeRelease();

  // Delete a file
  returnValue = deleteFile();
  if (returnValue != 0) {
    return returnValue;
  }
  waitForExe();
  waitForExeRelease();

  return 0;
}

void metadata() {
  eadk_display_wait_for_vblank();
  EADK::Display::pushRectUniform(EADK::Screen::Rect, White);

  EADK::Display::pushRectUniform(EADK::Rect(0, 0, 320, 18), Yellow);
  const char * title = "STORAGE";
  EADK::Display::drawString(title, EADK::Point((320 / 2) - (strlen(title) * 7 / 2), 3), false, White, Yellow);

  // Get the free space
  uint32_t used = extapp_used();
  uint32_t storageSize = extapp_size();
  uint32_t freeSpace = storageSize - used;

  uint32_t percentage = used * 100 / storageSize;
  uint8_t model = extapp_calculatorModel();

  char buffer[200];


  sprintf(buffer, "Used : %lu, Free : %lu", used, freeSpace);
  EADK::Display::drawString(buffer, EADK::Point(0, 22), true, Black, White);

  sprintf(buffer, "Total : %lu", storageSize);
  EADK::Display::drawString(buffer, EADK::Point(0, 42), true, Black, White);

  sprintf(buffer, "Percentage : %lu%%", percentage);
  EADK::Display::drawString(buffer, EADK::Point(0, 62), true, Black, White);

  if (model == 1) {
    EADK::Display::drawString("Model : N0110 or N0115", EADK::Point(0, 82), true, Black, White);
  } else if (model == 2) {
    EADK::Display::drawString("Model : N0120", EADK::Point(0, 82), true, Black, White);
  } else {
    EADK::Display::drawString("Model : Unknown (please report, should not happen)", EADK::Point(0, 82), true, Black, White);
  }
}

int main(int argc, char * argv[]) {
  // // Test the storage functions
  // int returnValue = testStorage();
  // if (returnValue != 0) {
  //   return returnValue;
  // }
  // waitForExe();
  // waitForExeRelease();

  // Show storage metadata
  metadata();
  waitForExe();
  waitForExeRelease();

  return 0;
}